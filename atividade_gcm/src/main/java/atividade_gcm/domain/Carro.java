package atividade_gcm.domain;

public class Carro {

	private Integer id;
	private String modelo;
	private Proprietario proprietario;
	private String placa;
	
	public Carro(Integer id, String modelo, Proprietario proprietario, String placa) {
		super();
		this.id = id;
		this.modelo = modelo;
		this.proprietario = proprietario;
		this.placa = placa;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public Proprietario getProprietario() {
		return proprietario;
	}
	public void setProprietario(Proprietario proprietario) {
		this.proprietario = proprietario;
	}
	
	
}
